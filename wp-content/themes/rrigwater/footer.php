<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage RRIG_WATER
 * @since 1.0.0
 */

?>
<div style="clear: both; height: 70px; width: 100%; display: block">&nbsp;</div>
<div class="questionfooter"><span style="font-size:40px;font-weight: normal;letter-spacing: normal;">Questions?</span><br>
GIVE US A CALL AT 817-887-9371</div>
	</div><!-- #content -->
<div id="bluebar"></div>
	<footer id="colophon" class="site-footer">

		<div class="site-info">
			<?php $blog_info = get_bloginfo( 'name' ); ?>
		<?php get_template_part( 'template-parts/footer/footer', 'widgets' ); ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->

</div><!-- #page -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>

			 $('#showmenu').on("click", function() {

  	if( $('.site-menu').hasClass('openmenu') ){

  		$('.site-menu').removeClass('openmenu');
  		$('#showmenu').removeClass('closeit');

  	} else {
  		
  		$('.site-menu').addClass('openmenu');
  		$('#showmenu').addClass('closeit');
  	}
  });

	
$(".site-header.featured-image .site-featured-image .post-thumbnail img").addClass("growup");
$(".entry-title").addClass("welcomehome");

/* menu swap */

$(document).scroll(function() {

  var scrollTop = $(window).scrollTop();
       console.log(scrollTop);
        if (scrollTop >= 200 ) {
            $('#site-navigation').addClass("fixedmenu");
        }
        else{
            $('#site-navigation').removeClass("fixedmenu");
        }
});
	
	
	
	
$(document).ready(function(){


	
	
	
$(".entry-content *").each(function(i, el) {
    var el = $(el);
    if (el.isInViewport(true)) {
      el.addClass("slideup"); 
    } 
  });
});
$(window).on('resize scroll', function() {


$(".entry-content *").each(function(i, el) {
    var el = $(el);
    if (el.isInViewport(true)) {
      el.addClass("slideup"); 
    } 
  });

});


$.fn.isInViewport = function() {
var elementTop = $(this).offset().top;
var elementBottom = elementTop + $(this).outerHeight();
var viewportTop = $(window).scrollTop();
var viewportBottom = viewportTop + $(window).height();
return elementBottom > viewportTop && elementTop < viewportBottom;
};


		</script>
<?php wp_footer(); ?>

</body>
</html>
