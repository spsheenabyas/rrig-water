<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage RRIG_WATER
 * @since 1.0.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo get_bloginfo('url') ?>/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_bloginfo('url') ?>/apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_bloginfo('url') ?>/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_bloginfo('url') ?>/apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo get_bloginfo('url') ?>/apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo get_bloginfo('url') ?>/apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo get_bloginfo('url') ?>/apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo get_bloginfo('url') ?>/apple-touch-icon-152x152.png" />
<link rel="icon" type="image/png" href="<?php echo get_bloginfo('url') ?>/favicon-196x196.png" sizes="196x196" />
<link rel="icon" type="image/png" href="<?php echo get_bloginfo('url') ?>/favicon-96x96.png" sizes="96x96" />
<link rel="icon" type="image/png" href="<?php echo get_bloginfo('url') ?>/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="<?php echo get_bloginfo('url') ?>/favicon-16x16.png" sizes="16x16" />
<link rel="icon" type="image/png" href="<?php echo get_bloginfo('url') ?>/favicon-128.png" sizes="128x128" />
<link href="https://fonts.googleapis.com/css?family=Spectral|Zilla+Slab:500" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Arvo:400,400i,700,700i" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'rrigwater' ); ?></a>

		<header id="masthead" class="<?php echo is_singular() && rrigwater_can_show_post_thumbnail() ? 'site-header featured-image' : 'site-header'; ?>">

			<div class="site-branding-container">
				<?php get_template_part( 'template-parts/header/site', 'branding' ); ?>
			</div><!-- .layout-wrap -->

			<?php if ( is_singular() && rrigwater_can_show_post_thumbnail() ) : ?>
				<div class="site-featured-image">
					<?php
						rrigwater_post_thumbnail();
						the_post();
						$discussion = ! is_page() && rrigwater_can_show_post_thumbnail() ? rrigwater_get_discussion_data() : null;

						$classes = 'entry-header';
					if ( ! empty( $discussion ) && absint( $discussion->responses ) > 0 ) {
						$classes = 'entry-header has-discussion';
					}
					?>
					<div class="<?php echo $classes; ?>">
						<?php get_template_part( 'template-parts/header/entry', 'header' ); ?>
					</div><!-- .entry-header -->
					<?php rewind_posts(); ?>
				</div>
			<?php endif; ?>
		</header><!-- #masthead -->

	<div id="content" class="site-content">
