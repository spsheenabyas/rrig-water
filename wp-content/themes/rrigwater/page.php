<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage RRIG_WATER
 * @since 1.0.0
 */

get_header();
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				
			endwhile; // End of the loop.
			?>
		<?php if(is_front_page()){
			?>
			<div class="iconscontainer">
			<?php
//,'order' => 'DESC'
			$args = array( 'post_type' => 'hpicons','order' => 'ASC', 'posts_per_page' => 12);
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); 
				
				$thetitle = get_the_title();
				$thetxt = get_the_content();
				?>
				<div class="iconbox">
					<div class="circle">
						<div class="circletext">
				<?php echo $thetxt; ?></div>
						<div class="iconimg">
				<?php the_post_thumbnail( 'full', 'url' ); ?></div>
					</div>

				<div class="icontitle"><?php echo $thetitle;?></div>
					
				</div>
				<?php
			endwhile;
			wp_reset_query(); 

		?>
		</div>
		<?php
		} ?>
		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
